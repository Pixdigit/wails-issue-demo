package main

import (
	"fmt"

	"github.com/leaanthony/mewn"
	"github.com/wailsapp/wails"
)

type Runtime struct {
	n string
}

func (r *Runtime) WailsInit(runtime *wails.Runtime) error {
	runtime.Events.On("test", log)
	return nil
}

func log(test ...interface{}) {
	s := fmt.Sprint(test)
	fmt.Println("Log: \"" + s + "\"")
}

func main() {

	js := mewn.String("./frontend/dist/app.js")
	css := mewn.String("./frontend/dist/app.css")

	app := wails.CreateApp(&wails.AppConfig{
		Width:  1024,
		Height: 768,
		Title:  "My Project",
		JS:     js,
		CSS:    css,
		Colour: "#131313",
	})
	app.Bind(&Runtime{})
	app.Run()
}
